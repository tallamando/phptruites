<link rel="stylesheet" href="style.css">
<?php if ((!isset($_POST['idBassin']))) {
    // Code de supprBassin 
?>
    <?php $titre = "Suppr bassin"; ?>
    <?php ob_start(); ?>
    
    <?php

    //Requete SQL
    require "bdd/bddconfig.php";
    session_start();
    if (isset($_SESSION['logged_in']['login']) !== TRUE) {
        // Redirige vers la page d'accueil (ou login.php) si pas authentifié
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    }
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bassins = $objBdd->query("select * from bassin");
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
    ?>
    <article>
        <h1>Les bassins :</h1>
        <table>
            <thead>
                <tr>
                    <th>Nom bassin</th>
                    <th>Supprimer</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($bassins as $bassin) { ?>
                    <tr>
                        <td><?php echo $bassin['nom']; ?></td>
                        <td>
                            <form method="POST" action="supprbassin.php">
                                <input type="hidden" name="idBassin" value="<?php echo $bassin['idBassin']; ?>">
                                <input type="submit" value="Supprimer">
                            </form>
                        </td>
                    </tr>
                <?php
                } //fin foreach
                $bassins->closeCursor(); //libère les ressources de la bdd
                ?>
            </tbody>
        </table>
    </article>


    <?php $contenu = ob_get_clean(); ?>
    <?php require 'gabarit/template.php'; ?>
<?php } else {
    // Code de deletebassin 
?>
    <?php
    require "bdd/bddconfig.php";

    $paramOK = false;
    // Recup la variables POST et les sécurise
    if ((isset($_POST['idBassin']))) {
        $idBassin = intval(htmlspecialchars($_POST['idBassin']));
        $paramOK = true;
    }

    // INSERT dans la base
    if ($paramOK == true) {
        try {
            $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
            $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Supprimer les températures de ce bassin
            $delBassin = $objBdd->prepare("DELETE FROM temperature WHERE idBassin =:id");
            $delBassin->bindParam(':id', $idBassin, PDO::PARAM_INT);
            $delBassin->execute();

            //Supprimer le bassin de la table bassin
            $delBassin = $objBdd->prepare("DELETE FROM bassin WHERE idBassin =:id");
            $delBassin->bindParam(':id', $idBassin, PDO::PARAM_INT);
            $delBassin->execute();


            // Redirige vers une page différente du dossier courant
            $serveur = $_SERVER['HTTP_HOST'];
            $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $page = 'bassins.php';

            header("Location: http://$serveur$chemin/$page");
            //header("Location: http://localhost:82/formulaires/index.php");
        } catch (Exception $prmE) {
            die('Erreur : ' . $prmE->getMessage());
        }
    } else {
        die('Les paramètres reçus ne sont pas valides');
    } ?>
<?php } ?>