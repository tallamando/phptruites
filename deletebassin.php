<?php
    require "bdd/bddconfig.php";
    session_start();
//récupérer les 3 variables POST
//sécuriser les variables reçues
$paramOK = false;

if(isset($_POST["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_POST["idbassin"]));
    $paramOK = true;
    }

if ($paramOK == true) {

//INSERT dans la base7
    try{
        //supprimer les températures liées au bassin
        $objBdd = new PDO ("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //supprimer les températures liées au bassin
        $pdoStmt = $objBdd->prepare("DELETE FROM temperature WHERE idBassin=:id");
        $pdoStmt ->bindParam(':id',$idbassin, PDO::PARAM_INT);
        $pdoStmt ->execute();
        
        //supprimer le bassin de la table bassins
        $pdoStmt = $objBdd->prepare("DELETE FROM bassin WHERE idBassin=:id");
        $pdoStmt ->bindParam(':id',$idbassin, PDO::PARAM_INT);
        $pdoStmt ->execute();

    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }

//rediriger automatiquement vers la page qui liste les bassins
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");


 }else{
     die("Les paramètres reçus ne sont pas valides.");
 }

?>