<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $titre ?></title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div id="conteneur">
            <header>
            <h1>La pisciculture PHP</h1>
                <div class="login"><?php 
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo 'Salut à toi, '.$_SESSION['logged_in']['prenom'].' '.$_SESSION['logged_in']['nom'].'! ';
                    //affichage "se déconnecter"(logout.php), "prif", "paramètres", etc.
                    ?><a href="logout.php">Se déconnecter</a><?php
                }else{
                    //Personne n'est authentifié
                    //affichage d'un lien pour se connecter
                    ?>
                    <a href="login.php">Connexion</a>
                    <?php
                }
                
                ?></div>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="bassins.php">Les bassins</a></li>
                    <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                    <?php if (isset($_SESSION['logged_in']['login']) == TRUE) { ?>
                        <li><a href="ajouterbassin.php">Ajouter un bassin</a></li>
                        <li><a href="supprbassin.php">Supprimer un bassin</a></li>
                    <?php } ?>
                </ul>
            </nav>
            <section>

            <?php echo $contenu ; ?>

            </section>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>