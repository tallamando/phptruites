<?php
    require "bdd/bddconfig.php";
    session_start();
//récupérer les 3 variables POST
//sécuriser les variables reçues
$paramOK = false;

if(isset($_POST["nom"])) {
    $nom = htmlspecialchars($_POST["nom"]);
    if (isset($_POST["descript"])) {
        $descript = htmlspecialchars($_POST["descript"]);
        if (isset($_POST["refcapteur"])) {
            $refcapteur = htmlspecialchars($_POST["refcapteur"]);
            $paramOK = true;
        }
    }
}
 if ($paramOK == true) {


//INSERT dans la base7
    try{
        $objBdd = new PDO ("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $pdoStmt = $objBdd->prepare("INSERT INTO bassin (nom, description, refCapteur) VALUES(:nom, :descript, :refcapteur)");
        $pdoStmt ->bindParam(':nom',$nom, PDO::PARAM_STR);
        $pdoStmt ->bindParam(':descript', $descript, PDO::PARAM_STR);
        $pdoStmt ->bindParam(':refcapteur', $refcapteur, PDO::PARAM_STR);
        $pdoStmt ->execute();

        $lastID = $objBdd->lastInsertId();
        echo $lastID;


    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }

//rediriger automatiquement vers la page qui liste les bassins
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");


 }else{
     die("Les paramètres reçus ne sont pas valides.");
 }



?>