<link rel="stylesheet" href="style.css">
<?php $titre = "Températures"; 
session_start();
$idBassin = 0;
$nomBassin = "Bassin inconnu";
$idok = isset($_GET['idBassin']);
$nomok = isset($_GET['nomBassin']);

if(($idok == true) && ($nomok == true)){
    $idBassin = intval(htmlspecialchars($_GET['idBassin']));
    $nomBassin = htmlspecialchars($_GET['nomBassin']);
}
/*Requête SQL*/
require 'bdd/bddconfig.php';
$objBDD = new PDO("mysql:host=$bddserver;
                dbname=$bddname;
                charset=utf8", $bddlogin, $bddpass);

$listeTemperatures = $objBDD->prepare("SELECT * FROM temperature WHERE idBassin = :id ORDER BY date DESC");
$listeTemperatures->bindParam(':id', $idBassin, PDO::PARAM_INT);
$listeTemperatures->execute();

?>
<?php ob_start(); ?>

<article>
    <h1>Températures du <?= $nomBassin; ?></h1>
<?php echo "idBassin = ".$idBassin; ?>

    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Température (°C)</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($listeTemperatures as $temp) { ?>
            <tr>
                <td><?php echo $temp['date'];?></td>
                <td><?php echo $temp['temp'];?></td>
            </tr>
            <?php
            }/*fin foreach*/
            $listeTemperatures->closeCursor(); ?>
        </tbody>
    </table>

</article>

<?php $contenu = ob_get_clean();?>
<?php require 'gabarit/template.php'?>