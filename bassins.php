<?php $titre = "Bassins";?>
<?php
require "bdd/bddconfig.php";
session_start();
try {
    $objBdd = new PDO("mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8", $bddlogin, $bddpass);
            $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           $listeBassins = $objBdd->query("SELECT * FROM bassin");
           } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }?>
<?php ob_start(); ?>
<article>
    <h1>Les bassins</h1>
    <?php
    while ($bassin = $listeBassins->fetch()) {
    ?>

        <h2><?php echo $bassin['nom']; ?></h2>
        <p><?= $bassin['description']; ?></p>
        <img src="images/<?php echo $bassin['photo']?>">
        <p><a href="temperatures.php?idBassin=<?= $bassin['idBassin']?>&nomBassin=<?= $bassin['nom']?>">Voir les températures</a></p>
        

    <?php
    }//fin du while
    $listeBassins->closeCursor(); //libère les ressources de la BDD
    ?>
</article>
<?php $contenu=ob_get_clean()?>
<?php require 'gabarit/template.php'?>
