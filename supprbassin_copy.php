<link rel="stylesheet" href="style.css">
<?php $titre = "Bassins";?>
<?php
require "bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8", $bddlogin, $bddpass);
            $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           $bassins = $objBdd->query("SELECT * FROM bassin");
           }
    catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage()); }?>
<?php ob_start(); ?>
<article>
    <h1>Supprimer un bassin</h1>
    <table>
        <thead>
            <th>Bassin</th>
            <th>Suppression</th>
        </thead>
        <tbody>
            <?php foreach ($bassins as $bassin) { ?>
                <tr>
                    <td><?php echo $bassin['nom']; ?></td>
                    <td>
                        <form method="POST" action="deletebassin.php">
                            <input type="hidden" name="idbassin" value="<?php echo $bassin['idBassin']; ?>">
                            <input type="submit" value="Supprimer">
                        </form>
                    </td>
                </tr>
                <?php
            } //fin foreach
            $bassins->closeCursor(); //libère les ressources de la bdd
            ?>
        </tbody>
    </table>
</article>
<?php $contenu=ob_get_clean()?>
<?php require 'gabarit/template.php'?>
